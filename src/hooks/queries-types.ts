export enum QueryType {
  Covid = "Covid",
}



export type CovidQuerySort =
  | "TotalConfirmed"
  | "TotalDeaths"
  | "TotalRecovered"
  | "NewConfirmed"
  | "NewDeaths"
  | "NewRecovered"
  | "Country"
export interface ICovidQuery {
  searchText?: string
  sort?: CovidQuerySort
  slice?: number
}
